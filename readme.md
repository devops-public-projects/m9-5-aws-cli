
# Interacting with AWS CLI

## Technologies Used
- AWS
- Linux

## Project Description
- Install and configure AWS CLI tool to connect to our AWS account
- Create EC2 Instance using the AWS CLI with all necessary configurations like Security Group
- Create SSH key pair
- Create IAM resources like User, Group, Policy using the AWS CLI
- List and browse AWS resources using the AWS CLI

## Prerequisites
- A computer to install AWS CLI on
- AWS Account

## Guide Steps
### Install and Configure AWS CLI
- Depending on your operating system utilize the install steps from the [AWS CLI install article](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- `aws configure`
	- AWS Access Key ID: **From your AWS account user Security Credentials**
	- AWS Secret Access Key: **From your AWS account user Security Credentials**
	- Default Region: [**Closest Region to you**](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.RegionsAndAvailabilityZones.html)
		- **Note**: Make sure you configure your CLI to use the region where you have created your EC2 instances and security groups!
	- Default output format: **json**
### Creating an EC2 Instance via CLI
- We will utilize the below command to make an EC2 instance. First we will need some information to fill in the missing portions.
```bash
aws ec2 run-instances
--image-id ami-xxxxxxxx
--count 1
--instance-type t2.micro
--key-name MyKeyPair
--security-group-ids sg-xxxxxxx
--subnet-id subnet-xxxxxxx
```
#### Setting up a Security Group
- Get our VPC ID
	- `aws ec2 describe-vpcs`
- Create Group
	- `aws ec2 create-security-group --group-name my-sg --description "My SG" --vpc-id VPC_ID`

![Successful Security Group Creation](/images/m9-5-successful-security-group-creation.png)

- Configure Security Group
```bash
aws ec2 authorize-security-group-ingress \
--group-id SECURITY_GROUP_ID \
--protocol tcp \
--port 22 \ 
--cidr YOUR_IP_ADDRESS/32
```
![Configured Security Group](/images/m9-5-configured-security-group.png)

#### Create a Key Pair
```bash
aws ec2 create-key-pair \
--key-name MyKpCli \
--query 'KeyMaterial'
--output text > MyKpCli.pem
```
- This will create a new Key-Pair called `MyKpCli` and download an unencrypted `.pem` file with the key in it. We will use it to SSH into the instance when we create it later in this project.

#### Get a Subnet
- `aws ec2 describe-subnets`
	- We'll use the first one from the list

#### Get an Image
- `aws ec2 describe-images` or from the UI choose the default Amazon Linux version from **Launch Instances** menu in EC2.
	- We'll use `ami-0230bd60aa48260c6` since it's the default Amazon Linux image right now

#### Create the EC2 Instance
```bash
aws ec2 run-instance \
--image-id ami-0230bd60aa48260c6 \
--count 1 \
--instance-type t2.micro \
--key-name MyKpCli \
--security-group-ids SECURITY_GROUP_ID \
--subnet-id SUBNET_ID
```
- The output will give you information on the new EC2 instance, you can verify it was created via the UI or via CLI with the below command.
	- `aws ec2 describe-instances`

![Successful EC2 Instance Creation](/images/m9-5-created-ec2-instance.png)

### SSH Into New EC2 Instance
- Obtain the public IP from either the output after creation of the EC2 instance, or from the UI.
- Move the **.pem** file and restrict access
	- `mv ~/MyKpCli.pem ~/.ssh`
	- `chmod 400 ~/.ssh/MyKpCli.prm`
- `ssh -i ~/MyKpCli.pem ec2-user@PUBLIC_IP`

![Successful SSH Session](/images/m9-5-successful-ec2-ssh.png)


### Browse Our EC2 Instances
- Show All Instances that have t2.micro instance-type and only show the InstanceId's that are relevant.
```bash
aws ec2 describe-instances \
--filters “Name=instance-type,Values=t2.micro” \
--query “Reservations[].Instances[].InstanceId”
```
